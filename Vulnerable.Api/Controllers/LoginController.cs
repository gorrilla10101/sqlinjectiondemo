﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Data;
using System.IO;

namespace Vulnerable.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly ILogger<LoginController> logger;

        public LoginController(ILogger<LoginController> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        public IActionResult Login([FromBody] LoginModel user)
        {
            Initialize();
            using var connection = new SQLiteConnection("Data Source=mydb.db");
            connection.Open();
            using var command = connection.CreateCommand();
            string query = "Select username, password from users where username='" + user.Name + "' AND password='" + user.Password + "'";
            logger.LogInformation("Query Is: {query}", query);

            command.CommandText = query;
            using var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                logger.LogInformation("User Logged In.");
                return Ok();

            }
            logger.LogInformation("Incorrect username or password.");
            return Unauthorized();
        }

        private static void Initialize()
        {
            if (!System.IO.File.Exists("mydb.db"))
            {
                SQLiteConnection.CreateFile("mydb.db");
            }
            using var connection = new SQLiteConnection("Data Source=mydb.db");
            connection.Open();

            using var command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY ASC, username, password)";
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();

            command.CommandText = "INSERT INTO users(username, password) VALUES($username, $password)";
            var userName = command.CreateParameter();
            userName.ParameterName = "$username";
            userName.Value = "SuperExtremeAdministrator";

            var password = command.CreateParameter();
            password.ParameterName = "$password";
            password.Value = "99kkaafi#(9**FR";

            command.Parameters.Add(userName);
            command.Parameters.Add(password);
            command.ExecuteNonQuery();


            command.CommandText = "CREATE TABLE IF NOT EXISTS bankaccounts(id INTEGER PRIMARY KEY ASC, name TEXT, accountnumber TEXT, routingnumber TEXT)";
            command.CommandType = CommandType.Text;
            command.Parameters.Clear();
            command.ExecuteNonQuery();

            command.CommandText = "INSERT INTO bankaccount(name, accountnumber, routingnumber) VALUES($name, $accountnumber, $routingnumber)";
            var bank = command.CreateParameter();
            bank.ParameterName = "$name";
            bank.Value = "Name of Bank";

            var account = command.CreateParameter();
            account.ParameterName = "$accountnumber";
            account.Value = "aklj34kj";

            var routing = command.CreateParameter();
            routing.ParameterName = "$routingnumber";
            routing.Value = "kdkfjo";

            command.Parameters.Add(bank);
            command.Parameters.Add(account);
            command.Parameters.Add(routing);


        }
    }

    public record LoginModel(string Name, string Password);
}
