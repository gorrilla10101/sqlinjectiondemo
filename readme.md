# About

This repository and a demonstration for what allows an sql injection attack, some ways that it can be achieved.

## Vulnerable.Api

The Vulnerable.Api is an intentionally vulnerable api to explore sql injection with. It uses and sqllite database that is recreated if destroyed.

## Endpoints

1. <https://localhost:5001/swagger/index.html>
2. <https://localhost:5001/login>

Swagger is an api documenter and can be used to send requests to the api.  

The /login endpoint is the vulnerable and expecting a username and password payload.

```json
{
  "name": "SuperExtremeAdministrator' or 1=1;--",
  "password": "string"
}
```

## Attacker.Console

The Attacker.Console is a console application that automates the attack and discovers all of the tables and columns listed in the database.
It outputs two files

1. fullattack.txt - has all queries that were executed during the attack.

2. tablecolumns.txt - lists tables and columns discovered during the attack.

